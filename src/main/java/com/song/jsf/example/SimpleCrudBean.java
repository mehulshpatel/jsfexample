package com.song.jsf.example;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Component;

import com.song.jsf.example.service.EducationLevelService;
import com.song.jsf.example.util.SortColumnData;

@ManagedBean
@SessionScoped
@Component(value = "simpleCrudBean")
public class SimpleCrudBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Student> list;
	private Student item = new Student();
	private Student beforeEditItem = null;

	private SortColumnData sortColumnData;

	private EducationLevelService educationLevel;

	private boolean edit;

	@PostConstruct
	public void init() {
		list = new ArrayList<Student>();
		sortColumnData = new SortColumnData();
		educationLevel = new EducationLevelService();
	}

	public void add() {
		item.setId(list.isEmpty() ? 1 : list.get(list.size() - 1).getId() + 1);
		list.add(item);
		item = new Student();
	}

	public void resetAdd() {
		item = new Student();
	}

	public void edit(Student item) {
		beforeEditItem = item.clone();
		this.item = item;
		edit = true;
	}

	public void cancelEdit() {
		this.item.restore(beforeEditItem);
		this.item = new Student();
		edit = false;
	}

	public void saveEdit() {
		this.item = new Student();
		edit = false;
	}

	public void delete(Student item) throws IOException {
		list.remove(item);
	}

	public List<Student> getList() {
		return list;
	}

	public Student getItem() {
		return this.item;
	}

	public boolean isEdit() {
		return this.edit;
	}

	public SortColumnData getSortColumnData() {
		return sortColumnData;
	}

	public void setSortColumnData(SortColumnData sortColumnData) {
		this.sortColumnData = sortColumnData;
	}

	public EducationLevelService getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(EducationLevelService educationLevel) {
		this.educationLevel = educationLevel;
	}

}