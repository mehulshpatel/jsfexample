package com.song.jsf.example.util.constant;

public enum EducationLevelEnum {
	ELEMENTARY,
	SECONDARY,
	HIGHER
}
