package com.song.jsf.example.exception;

import javax.validation.ValidationException;

public class StudentNotAllowedException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public StudentNotAllowedException() {
		super("Student below 13 are not allowed");
	}
}
