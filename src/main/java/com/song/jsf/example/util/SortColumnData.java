package com.song.jsf.example.util;

import org.richfaces.component.SortOrder;

public class SortColumnData {
	
	private SortOrder nameOrder = SortOrder.unsorted;
    private SortOrder ageOrder = SortOrder.unsorted;
    
    public void sortByName() {
    	ageOrder = SortOrder.unsorted;
    	if(nameOrder.equals(SortOrder.ascending)) {
    		setNameOrder(SortOrder.descending);
    	} else { 
    		setNameOrder(SortOrder.ascending);
    	}
    }
    
    public void sortByAge() {
    	nameOrder = SortOrder.unsorted;
    	if(ageOrder.equals(SortOrder.ascending)) {
    		setAgeOrder(SortOrder.descending);
    	} else { 
    		setAgeOrder(SortOrder.ascending);
    	}
    }
    
    public SortOrder getNameOrder() {
		return nameOrder;
	}

	public void setNameOrder(SortOrder nameOrder) {
		this.nameOrder = nameOrder;
	}

	public SortOrder getAgeOrder() {
		return ageOrder;
	}

	public void setAgeOrder(SortOrder ageOrder) {
		this.ageOrder = ageOrder;
	}

}
