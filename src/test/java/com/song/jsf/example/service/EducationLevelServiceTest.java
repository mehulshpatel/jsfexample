package com.song.jsf.example.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.validation.ValidationException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.song.jsf.example.Student;
import com.song.jsf.example.exception.StudentNotAllowedException;
import com.song.jsf.example.util.constant.EducationLevelEnum;

public class EducationLevelServiceTest {
	
	private EducationLevelService educationLevelService = new EducationLevelService();

	private static Student student;
	
	@BeforeAll
	public static void setUp() {
		student = new Student(1L, "Tom", 15);
	}
	
	@DisplayName("Test For Elementary")
	@Test
	public void testEducationLevelForElementaryAgeWithSuccess() {
		student.setAge(14);
		String educationLevel = educationLevelService.calculateEducationLevel(student);
		assertEquals(EducationLevelEnum.ELEMENTARY.toString(), educationLevel);
	}
	
	@DisplayName("Test For Secondary")
	@Test
	public void testEducationLevelForSecondaryAgeWithSuccess() {
		student.setAge(19);
		String educationLevel = educationLevelService.calculateEducationLevel(student);
		assertEquals(EducationLevelEnum.SECONDARY.toString(), educationLevel);
	}
	
	@DisplayName("Test For Higher")
	@Test
	public void testEducationLevelForHigherAgeWithSuccess() {
		student.setAge(37);
		String educationLevel = educationLevelService.calculateEducationLevel(student);
		assertEquals(EducationLevelEnum.HIGHER.toString(), educationLevel);
	}

	@DisplayName("Test For Exception")
	@Test()
	public void testEducationLevelForNotAllowedAgeThrowsException() {
		student.setAge(11);
		Exception exception = assertThrows(ValidationException.class, new Executable() {
			
			@Override
			public void execute() throws Throwable {
				educationLevelService.calculateEducationLevel(student);
			}
		}); 
		assertEquals(new StudentNotAllowedException().getMessage(), exception.getMessage());
	}
}
