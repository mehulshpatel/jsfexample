package com.song.jsf.example.service;

import javax.validation.ValidationException;

import com.song.jsf.example.Student;
import com.song.jsf.example.exception.StudentNotAllowedException;
import com.song.jsf.example.util.constant.EducationLevelEnum;

public class EducationLevelService {

	public String calculateEducationLevel(Student item) throws ValidationException{
    	if(item.getAge()>12 && item.getAge()<=16) {
    		return EducationLevelEnum.ELEMENTARY.toString();
    	} else if(item.getAge()>16 && item.getAge()<=22) {
    		return EducationLevelEnum.SECONDARY.toString();
    	} if(item.getAge()>22) {
    		return EducationLevelEnum.HIGHER.toString();
    	} 
    	throw new StudentNotAllowedException();
    }
}
 