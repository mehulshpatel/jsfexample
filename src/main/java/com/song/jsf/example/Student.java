package com.song.jsf.example;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull
	private String name;
	
	@Min(value = 13, message = "Student below 13 are not allowed")
	private Integer age;
	
	@Override
	public Student clone() {
		return new Student(id, name, age);
	}

	public void restore(Student student) {
		this.id = student.getId();
		this.name = student.getName();
		this.age = student.getAge();
	}
}
